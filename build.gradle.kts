import org.openapitools.generator.gradle.plugin.tasks.GenerateTask

plugins {
    java
    id("org.openapi.generator") version "5.4.0"
    id("maven-publish")
    id("com.diffplug.spotless") version "6.4.2"
}

val type = GenerationType.valueOf(project.properties.getOrDefault("type", "Client") as String)

enum class GenerationType {
    Server,
    Client
}

val artifactName = when (type) {
    GenerationType.Server -> "openapi.service.server"
    GenerationType.Client -> "openapi.service.client"
}

val groupName = "io.igolide.blueprints"
val versionValue = System.getenv("VERSION") ?: "0.0.0"
val mavenRegistry = "https://gitlab.com/api/v4/projects/36912133/packages/maven"

group = groupName
version = versionValue

val prepareVersion = "prepareVersion"
val javaClient = "javaClient"
val springServer = "springServer"
val spotless = "spotlessJavaApply"
val verifySpecification = "verifySpecification"

repositories {
    mavenCentral()
}

dependencies {
    if (type == GenerationType.Server) {
        implementation("org.springframework.boot:spring-boot-starter-web:2.6.2")
        implementation("org.springframework.data:spring-data-commons:2.6.0")
        implementation("org.springdoc:springdoc-openapi-ui:1.6.4")
        implementation("com.google.code.findbugs:jsr305:3.0.2")
        implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.13.1")
        implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.1")
        implementation("org.openapitools:jackson-databind-nullable:0.2.2")
        implementation("org.springframework.boot:spring-boot-starter-validation:2.6.2")
        implementation("com.fasterxml.jackson.core:jackson-databind:2.13.1")
    }
    if (type == GenerationType.Client) {
        implementation("javax.annotation:javax.annotation-api:1.3.2")
        implementation("io.swagger:swagger-annotations:1.6.6")
        implementation("org.openapitools:openapi-generator:5.4.0")
        implementation("com.squareup.okhttp3:okhttp:4.9.1")
        implementation("com.squareup.okhttp3:logging-interceptor:4.9.1")
        implementation("com.google.code.gson:gson:2.8.6")
        implementation("io.gsonfire:gson-fire:1.8.4")
    }
}

tasks.register(prepareVersion) {
    copy {
        from(layout.projectDirectory.file("specification/openapi.yaml"))
        into(layout.projectDirectory.dir("specification/temp"))
        filter {
            it.replace("VERSION", versionValue)
        }
    }
    delete {
        file("$rootDir/specification/openapi.yaml")
    }
    copy {
        from(layout.projectDirectory.file("specification/temp/openapi.yaml"))
        into(layout.projectDirectory.dir("specification"))
    }
    delete("$rootDir/specification/temp")
}

tasks.register(springServer, GenerateTask::class) {
    group = "openapi tools"
    generatorName.set("spring")
    groupId.set(groupName)
    inputSpec.set("$rootDir/specification/openapi.yaml")
    outputDir.set("$buildDir/generated")
    apiPackage.set("$groupName.$artifactName.service")
    invokerPackage.set("$groupName.$artifactName.invoker")
    modelPackage.set("$groupName.$artifactName.model")
    configOptions.set(
        mapOf(
            "dateLibrary" to "java8",
            "delegatePattern" to "true",
        )
    )
    doLast {
        copy {
            from(layout.buildDirectory.dir("generated/src/main"))
            into(layout.projectDirectory.dir("src/main"))
        }
    }
}
tasks.getByPath(springServer).dependsOn(prepareVersion)
tasks.getByPath(springServer).finalizedBy(spotless)

tasks.register(javaClient, GenerateTask::class) {
    group = "openapi tools"
    groupId.set(groupName)
    generatorName.set("java")
    inputSpec.set("$rootDir/specification/openapi.yaml")
    outputDir.set("$buildDir/generated")
    apiPackage.set("$groupName.$artifactName.service")
    invokerPackage.set("$groupName.$artifactName.invoker")
    modelPackage.set("$groupName.$artifactName.model")
    configOptions.set(
        mapOf(
            "dateLibrary" to "java8"
        )
    )
    doLast {
        copy {
            from(layout.buildDirectory.dir("generated/src/main"))
            into(layout.projectDirectory.dir("src/main"))
        }
        copy {
            from(layout.buildDirectory.dir("generated/docs"))
            into(layout.projectDirectory.dir("src/main/resources/docs"))
        }
        copy {
            from(layout.buildDirectory.file("generated/api/openapi.yaml"))
            into(layout.projectDirectory.dir("src/main/resources"))
        }
    }
}
tasks.getByPath(javaClient).dependsOn(prepareVersion)
tasks.getByPath(javaClient).finalizedBy(spotless)

tasks.register(verifySpecification, Task::class)
tasks.getByPath(verifySpecification).dependsOn(javaClient)
tasks.getByPath(verifySpecification).finalizedBy("jar")

spotless {
    java {
        googleJavaFormat("1.15.0").aosp().reflowLongStrings()
        removeUnusedImports()
        importOrder()
        endWithNewline()
        trimTrailingWhitespace()
        indentWithSpaces()
    }
}

tasks.withType<JavaCompile> {
    options.isDeprecation = true
}

tasks.withType<Jar> {
    archiveFileName.set("$artifactName.jar")
}

publishing {
    repositories {
        maven {
            url = uri(mavenRegistry)
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
    publications {
        register("mavenJava", MavenPublication::class) {
            artifactId = artifactName
            from(components["java"])
        }
    }
}
